% File with general header information.
% Like all the packages that we need to include and very common commands
% that we use.
\input{inc/header.tex}

% File with "general commands". Those are commands  and environments
% that we find useful throughout many papers (and other kinds of documents).
\input{inc/generalCommands.tex}

% File with "specific commands". Those are commands that may be rather
% specific to the current document (like shortcuts to some notation).
\input{inc/specificCommands.tex}

% Start of the actual document.
\begin{document}
  \selectlanguage{english}
  
  
  \section{Intro}
  
  Thank you very much for this intro.
  As already said, my first talk is on \emph{equilibrium finding and abstraction of games of imperfect information}, which ultimately makes this a talk on game theory, or even more generally, on artificial intelligence.


  \section{Libratus poker AI}
  
  The occasion which is the reason for this talk dates back to January 2018.
  That is when an article was published in the Science magazine with the catchy caption:
  \emph{``Superhuman AI for heads-up no-limit poker: Libratus beats top professionals.''}
  It essentially tells the workings of an AI system running on a supercomputer called \emph{Libratus}, developed by Prof. Tuomas Sandholm and his student Noam Brown and technically also some others at the Carnegie Mellon University in Pittsburgh.
  The big achievement of Libratus is that, actually already back in January 2017, it was able to consistently outperform four top-level Texas hold 'em players -- so to say the Ronaldos and Messis of Texas hold 'em -- over the course of 20 days and a total of 120,000 played hands, for the sake of statistical significance.
  And the humans actually had a strong incentive to win because they had the chance of earning more than a 100,000\$ each, all depending on their overall performance.
  
  So, while this result may be impressive all on its own, Sandholm and Brown did not develop this AI just because they are greedy, but because it has meaning on a more general level.
  That is, because the methods they used are more generally designed for a larger class of games, called \emph{imperfect information games}, which are games in which a player may not have access to all the information on the state of the game, as it would for example be the case in checkers, chess or go where all the relevant information is always clearly visible on the field.
  And poker in all its variants is \emph{the} benchmark when it comes to such games.
  
  The special challenge behind solving these kinds of games is that there is not just the one optimal strategy and that, at a given state in the game, it is not just enough or possible to look at the current state itself, but one also has to take into consideration the history of moves that lead to this state, which might give hints of the information that is missing.
  In particular, the methods that were example used for Google's AI AlphaGo are not directly applicable, although the usage of deep neural networks is of course still possible.
  
  However, and that what really caught my attention: Libratus does not use a single neural network.
  It is almost entirely designed on game theoretic principles.
  This is interesting for two reasons:
  First, game theoretic approaches are attractive because -- at least for certain classes of games -- they may provably converge to an optimal strategy.
  If our opponent differs from that strategy, we automatically win money in the long run.
  That analytical optimality property is something that would not be all that clear from a neural network approach.
  The second thing is that Libratus runs on a super computer, which also means that algorithmic game theory made a big leap forward and extensions to large-scale problems appear to be more valid now.
  
  Finally, it is important to mention that this topic is not just about games.
  It actually is the case, that many real-world situations can formally be modeled as games, and more often than not that would be imperfect information games than perfect information games.
  Examples for that would clearly be automated negotiations and trading, but also military applications and cyber-security, when for example there is a sender and a signal jammer, both with limited battery, and one wands to come up with an optimal sending strategy, and also medical treatment planning where humanity is sort of playing against some pathogen and the goal is to force it to undergo a chain of mutations that in the end makes it easier for medical doctors to treat it.
  
  This makes this whole topic pretty exciting and worth a closer inspection.
  So, let's have a closer look at the theory and solving of games.
  
  
  \section{Quick intro: Texas hold 'em poker}
  
  But, before we dig into the real fun part, I would like to first give a short explanation of how Texas hold 'em poker actually works, just in case someone in this room is not fully familiar with it.
  A deep understanding of poker is neither crucial nor necessary for the remainder of this talk, but it might be still helpful to understand certain portions better.
  
  The base principle is quite simple:
  Each player gets to private cards, then there are further public cards added, which both players may use to combine their hand into one that is as strong as possible.
  In the end, the player with the strongest hand wins.
  In order to spice things up a little, there is some betting in between.
  
  So, in a tiny little more detail:
  Every player gets 2 private cards, that they keep to themselves.
  Then there is a betting round, where everyone is free to bet an arbitrary amount of many.
  The other players then either have to call, which means to match the bet, or fold and forfeit all of the investments they might have made during this round.
  Then three public cards are dealt, which everyone gets to see and builds a common basis for combining hands.
  There is another betting round,
  then another card is dealt, leading to a total of five public cards on the table,
  people bet once more,
  the final card is dealt,
  which also leads to the final betting round.
  If at this point there are still two players in the game, there is the showdown and the player who can form the best card combination using his own two cards and the five cards on the table wins all the money in the pot.
  
  While the game concept itself is relatively simple, the fact that there is no limit on the bet size can make the possible options in each step arbitrarily large, can also make the number of potential decisions arbirary large.
  
  Following a result of Johansom from 2013, the version of poker used for the big benchmarks with just two players has roughly $10^{161}$ different decision points, meaning the number of perceivably different situations any player may face.
  Just to put that in perspective, the estimated number of atoms in the observable universe is roughly of the order $10^80$.
  Or, the game of go on a 19-by-19 has roughly $10^{170}$ legal positions, but is of course missing the additional complexity stemming from the imperfect information property of poker.
  
  So, that sounds pretty frightening.
  How did Libratus tackle this problem?


  \section{Core concepts behind Libratus}
  
  To get a rough intuition, this schematic shows the three essential core concepts involved with the Libratus AI.
  
  A first important component is the offline-solver.
  It consists out of an abstraction algorithm for approximating the large game by a smaller one, then solving the (usually still pretty large) abstract game using equilibrium finding algorithms.
  This yields a blueprint strategy that is used during the online phase.
  
  The second component is an endgame solver, which runs online and becomes active whenever a late stage of the game is reached, which has not been resolved well within the abstract game.
  
  Finally, a self-improver is the one piece that comes close to a machine learning component:
  During play, it detects weaknesses in the own strategy that are used by the opponents and sends them off to the abstraction component for further improvement of the blueprint strategy.
  
  So, let's just assume for a second that your department cuts all your funding and you now want to develop your own poker bot to make up for it.
  Where do you start?
  That would of course be the abstraction and equilibrium finding part because it is the basis for everything else.
  Therefore, let us make it our goal to understand a little more about how these pieces work.
  
  
  \section{What is a game of imperfect information?}
  
  First, we need to know what a game of imperfect information actually \emph{is}.
  To that end, let me introduce the class of so-called ``extensive form games''.
  An extensive form game consists out of six components:
  The first one is the game tree that describes the games structure.
  Quite intuitively, the vertices are states of the game and edges can be considered concrete actions that lead from one state to another.
  Each vertex, except those on the leaf level, is associated to one of the players.
  By convention, player zero is the so-called chance player, which models the nature or luck component of the game.
  So far, this just the same as you would except it for perfect information games.
  What is unique to imperfection games are the so-called information sets.
  They model those states that a player is unable to tell apart because he or she is missing some information.
  Mathematically, this is described by a partition of a player's nodes $P_k$, where two nodes that are in the same partition set, also called information set, are considered indistinguishable by player $k$.
  Then, for each information set $I$, there is also a set of actions, where each game state in the same information set must admit the same actions, which in particular corresponds to a partition of th edges going out of the information set $I$.
  Then, once a leaf node is reached, there is a vector of pay-off values for each player associated to it, which is described by the utility function $u$.
  Finally, it is assumed that the ``strategy'' of the chance player $0$ is already known, which means that one knows a priory for every chance state what actions are how likely to happen.
  This is described by the family of probability densities $p_I$.
  
  
  \section{Example: an extensive form game}
  
  To make this concept a little more accessible, here is also a very small example of an extensive form game, which encodes the different components with different colors.

  The game works as follows: Alice and Bob walk down a road. With probability $\frac{1}{4}$, luck -- i.\,e. player zero -- is on Alice's side and she sees a coin on the street and Bob does not notice.
  So she gets to keep the coin, giving here payoff $1$ and Bob $0$.
  With probability $\frac{3}{4}$, however, both see the coin simultaneously.
  Alice -- player 1 -- can either move left or right with here move.
  Also Bob, who is player 2, needs to move, but he cannot see Alice yet, so he must decide before he gets to know Alice's decision.
  That puts both possible scenarios in the same information set for Bob.
  Depending on Bob's decision and the state he is in, it is decided who gets the coin.
  
  The same way, also a poker game might be presented as such a tree, only with the difference, that that one would be way bigger.
  
  
  \section{Perfect recall in EFGs}
  
  For many of the theoretical results, a further property is required, called \emph{perfect recall}.
  Simply put, it just means that every player always needs to be able to remember all of their own moves that led to the current information set.
  Formally, this means that any path in the game tree may intersect any information set at most once, else they might feel like they are stuck in a loop without knowing how often they have been there.
  Furthermore, for any two nodes $x_n$ and $y_n$ in an information set and any path leading $(x_1,\dots,x_n)$ to one of these nodes, there is another path $(y_1,\dots,y_n)$, which leads to $y_n$ and where the player associated to that final node took the identical actions at all of his nodes along the path.
  This means what state in an information set is reached precisely depends only on the actions of the other players and never on the player who owns that information set himself.
  
  
  \section{Example: an EFG without perfect recall}
  
  Again, this concept is best illustrated by an example.
  In this tree the perfect recall property is \emph{not} fulfilled.
  It shows a similary game as before: Player~1 gets to choose left or right, then player~2 gets to choose up or down, not knowing what player~1 went for.
  However, then player~1 gets an extra round where he or she needs to decide for either forward or backward.
  And, here you see that all of these states are grouped into one information set again.
  This does not only mean that player~1 cannot tell apart player~2's decision, but that player~1 also forgot whether left or right was chosen up there.
  Hence, perfect recall is violated.


  \section{What is a strategy?}
  
  Now, knowing what a game is, we can start thinking about:
  How do we solve a game?
  And for that we first need to know what a solution should look like in the first place.
  That leads us to the term of \emph{strategies} for games.
  There are two different ways to look at strategies that people distinguish.
  
  The first one, which could be considered the classical view, are the \emph{pure strategies}.
  A pure strategy for player~$k$ simply is a really long vector that tells for each of his information sets which action he will take, and the set of all pure strategies is here denoted by $\cA_k$.
  So, this is like deciding at the very beginning of the game what action will be taken for every possible situation that may occur.
  A \emph{mixed pure strategy} then simply is a probability density function over the set of all pure strategies, meaning that player~$k$ would choose one pure strategy at random at the very beginning of the game.
  This is expressed as a map from the set of pure strategies~$\cA_k$ into the standard simplex whose dimension corresponds to the cardinality of~$\cA_k$.
  
  
  A different -- and in my opinion more intuitive -- way to look at strategies are the \emph{behavioral strategies}.
  A behavioral strategy for player~$k$ simply is a family of probability functions that for each information set~$I$ defines a probability distribution over the set of possible actions~$\cA_I$ at that information set.
  Again this is expressed by maps from $\cA_I$ into the standard simplices of corresponding cardinality.
  In contrast to pure and mixed strategies, this viewpoint corresponds rather to player~$k$ waiting until situation $I$ happens and then -- at random -- deciding locally what action is to be taken.
  
  If you now feel like those two concepts are not all that different, then you are indeed on to something.
  There is a fundamental result on the equivalence of mixed strategies and behavioral strategies from 1953 by Harold Kuhn -- by the way that's the same Kuhn as in the famous KKT conditions.
  It essentially states:
  If we are dealing with a perfect-recall game
  and given a mixed strategy~$p_k$ for each player,
  this induces a behavioral strategy~$\sigma_k$ for every player
  such that the induced probability distribution over leaf nodes is the same both for the mixed stragies~$p$ and the behavioral strategies~$\sigma$.
  
  The induced behavioral strategy for an action~$a$ is simply defined such that it takes the weighted ratio of all pure strategies that could reach the corresponding information set, denoted by $R(I)$, and that actually would take action~$a$, over all those pure strategies that may reach the associated information set.
  
  For this result the perfect-recall condition really is necessary, and, what I omitted here, you can also map behavioral strategies back to mixed strategies.
  So, in that setting one may easily switch bewteen pure and behavioral strategies without worrying too much.
  In what follows we will mostly be thinking in terms of behavioral strategies, but we also need the pure strategies.
  
  
  \section{What is a good strategy?}
  
  Now, knowing a space a feasible solutions, the next question is of course:
  What is a \emph{good} solution?
  
  That brings us to the famous concept of Nash equilibria.
  To that end, suppose in a slight abuse of notation that $u(\sigma)$ denotes the expected utility that is obtained for a behavioral strategy~$\sigma$.
  So, this is essentially the expectational value of the utility function $u$ over all leaf nodes subject to the probability density function that is induced by $\sigma$.
  
  A strategy $\sigma$ is then an $\eps$-Nash equilibrium if the following is true:
  For every player~$k$ and every strategy~$\wt{\sigma}$
  that only changes the strategy for player~$k$ and keeps the strategy for all other players fixed,
  the expected payoff of player~$k$ can at most increase by $\eps$.
  
  If $\eps$ is $0$, then $\sigma$ is called a Nash equilibrium in the classical sense.
  A Nash equilibrium then simply means that for a given strategy, no player has a good incentive to deviate from that strategy all on their own.
  This is kind of a Mexican standoff approach to optimality here.
  
  Now we have both a solution space and an optimality condition:
  Naturally, the next question is whether this concept makes sense at all, \ie does there exist a solution?
  And, indeed this is the case, and the proof of that essentially goes back to the famous work of John Nash from 1951:
  Every extensive form game with perfect recall has a Nash equilibrium.
  
  The proof uses two things:
  First Kuhn's theorem is used to argue that it is enough to work with mixed strategies, which are required for Nash's classical result.
  Then Nash's classical result is based on a fixed point theorem which shows that there must be a strategy such that every player's optimal response to the other players' strategies must be his current strategy already.
  This can be done either using Brouwsers fixed point theorem or, more elegantly, also using the Kakutani fixed point theorem.
  
  
  \section{Nash-equilibria in perfect-recall EFGs}
  
  So, of course Nash's existence proof is not constructive:
  Hence the remains the question, how do we compute a Nash equilibrium?
  
  For that I would like to restrict the attention to two player games where the one player's loss is always the other player's gain, which is also called a zero-sum game.
  This restriction is both for algorithmic reasons, but also because there are sort of philosophical reasons which make it unclear if Nash equilibria are a reasonable optimality concept in more general settings.
  
  The optimization problem associated to Nash equilibria reads as follows:
  Assuming the space of each players pure strategies has cardinality $m_k$, one can define  a payoff matrix $A$ which contains the utility for player~2 for every possible combination of pure strategies that the two players might go for.
  
  The optimal mixed strategy -- which we already know is equivalent to an optimal behavioral strategy as well -- then needs to solve the \emph{min-max matrix game problem}:
  It needs to a a pair $(x,y)$ such that $y$ maximizes the utility of player~2 given $x$, which simply means to maximize $x^TAy$, but also such that $x$ maximizes the utility of player~1 given $y$, which means to minimize $x^TAy$.
  
  
  \section{Nash-equilibria in perfect-recall EFGs (2)}
  
  In principle, solving this problem is actually not all that hard.
  Because it is equivalent to a linear program, which could be stated as follows:
  Let's maybe go from bottom to top here.
  We are searching in the space of all mixed strategies, which is equivalent to looking on a standard simplex and corresponds to the last two rows here.
  Then, if player~1 picks a mixed strategy~$x$, player~2 would ideally want to choose the best counter-strategy, which means of this matrix-vector product he could choose the one row with the maximum value and just play that pure strategy.
  This means, player~1 needs to minimize that maximum value~$M$ in order to find the strategy which gives the best expected utility for himself, even if player~2 is always able to play the perfect strategy against player~1.
  Similarly, the optimal strategy of player~2 is given by the dual linear program.
  
  Okay, so in principle everything here is pretty easy and there is nothing new really.
  However\emph{...} the big problem in application is the excessive size of $A$ in general, which renders the use of simplex or interior point methods infeasible.
  The solution of this issue takes us back to the research of the past decade.
  
  In order to tackle this problem, there have been various different approaches in the past couple years, most notably sparse and low-rank representations or approximations of $A$ are being used, there are also ways to formulate the problem not over the space of all pure strategies but a significantly smaller space, but most notably there were big improvements on iterative first order methods for solving matrix game problems, which we will now have a closer look at.
  
  
  \section{Duality gap minimization}
  
  The idea is to minimize the gap between a primal and a dual formulation of the problem.
  The primal problem is to optimize for player one's utility against a player two who may choose the perfect counter-strategy, which means minimizing the function~$f$ here.
  The dual problem~$\phi$ is essentially the same, but from player two's perspective: Maximize the utility while player two tries to counter that as much as possible.
  
  An optimal strategy is then obtained from a pair~$(x,y)$ that makes the gap~$F$ between $f$ and $\phi$ vanish.
  Therefore, we arrive at a minimization problem in~$F$ over the simplices $\Delta_1$ and $\Delta_2$.
  However, $F$ is not smooth.
  
  But smooth approximations are readily available, which simply result from a convex perturbation of $F$, like for example by a quadratic term as shown here.
  The perturbed function $F_{\mu}$ is then continuously differentiable with Lipschitz-continuous gardient and fulfills the uniform approximation property shown here.
  In particular, a minimizer of $F_\mu$ also yields an approximate minimizer of $F$.
  
  
  \section{An $\mathcal{O}(1/\eps)$ first order method}
  
  Actually, this idea relies on a more concept of Yurii Nesterov from 2005 for the smooth minimization of nonsmooth functions where he also developed a new first order method for such convex functions, which was later on in 2011 slightly improved by Lan, Lu and Monteiro.
  Its main features are that it has a improved rate of convergence compared to classical projected gradient methods, namele $1/k^2$ instead of just $1/k$, and that in the matrix game setting it is possible to get explicit bounds on the involved constants such that the maximum number of necessary iteration steps for finding an $\eps$-Nash equilibrium is known a priori.
  
  That method requires per iteration step four matrix-vector multiplications with the matrix $A$ and two projections onto the product of two standard-simplices.
  The projection onto a standard simplex is actually a well-known problem and can be solved with almost linear complexity.
  The matrix-vector multiplication is potentially more expensive, but if $A$ is sparse, this may come closer to a linear comlpexity, too.
  In that case, the complexity of each step would almost be linear, which is as good as one might reasonably expect to get.
  
  
  \section{Nesterov algorithm}
  
  Sadly, we do not have the time to go through that algorithm in detail, but in case you are curious, this is what the algorithm roughly looks like for the matrix game problem.
  The most essential part is, that the new iterates $x_k$ and $y_k$ stem from a sort of perturbed gradient-projection step with respect to some vectors $u_k$ and $v_k$, which again are defined from a linear combination of vectors $w_k, z_k$ and the old iterate $x_k, y_k$.
  Here $w_k$ and $z_k$, come from a sort-of Krylov-subspace correction term, which incorporates information from all past gradients.
  The problem stops once the unperturbed cost functional reaches an $\eps$-Nash equilibrium.
  
  
  \section{An $\mathcal{O}(\log(1/\eps))$ first order method}

  Now, the fact that there is an a priori known bound on the iteration count, makes one think:
  What if we adaptively adapt the smoothing factor $\mu$ and the value of $\eps$ in the algorithm?
  That is a direction, that Gilpin, Pe\~{n}a and Sandholm looked at in 2010, where they simply introduced a simple scheme which iteratively reduces $\eps$ by a constant factor of $1/e$ and then calls the Nesterov algorithm for that value of $\eps$.
  
  In that setting they are able to prove a logarithmic bound on the number of iterations \emph{inside} the Nesterov method, which is pretty exciting.
  Even more interestingly, their result involves what they call condition measure $\delta$, which after some re-formulation take this form.
  So, if you assume that the optimum is unique, then this essentially (almost) becomes the condition number for the inversion of $F$ in $0$, which some of you here might remember from the CoMaI lectures.
  Sadly, this is were the research stops:
  In particular, it is still completely unclear under what circumstances this condition measure is actually well-defined and when it is possible to obtain reasonable bounds on it.
  Furthermore, looking at that iteration bound: If it is indeed possible to control the condition number reasonably well, then it would also be interesting to find out if one can come up with preconditioning methods that might improve the efficiency of equilibrium finding.
  So, that's quite some tasty food for thought.
  
  
  \section{Game abstraction: conceptional overview}
  
  In that spirit, for the last few minutes that we have, I would like to direct your attention to yet another concept from game solving, which may look rather familiar to you as well: Game abstraction.
  The basic idea of game abstraction is pretty simple:
  We start with the full game, which is too expensive to solve, and therefore we restrict it to a smaller game.
  The abstract game is then small enough to allow our equilibrium finding algorithm to do its work and tell us an optimal strategy in the abstract game.
  Then, a prolongation step is performed, to map the abstract strategy to a strategy in the original game.
  Of course, ideally that prolongated strategy should also be close to an equilibrium again.


  \section{Restriction framework}
  
  Usually, such abstractions in game theory have in the past been hand-crafted by people with detailed knowledge on the problem at hand.
  However, there are also new approaches emerging, which try to tackle the problem of game abstraction algorithmically.
  Usually, when trying to abstract a game $G$ to a game $\wt{G}$, one makes decisions on how to cluster vertices, informations and actions into a new structure.
  Of course, there are certain consistency conditions between those maps, which I am omitting here.
  For now, let us just assume that we already are given the abstract game and know how to map onto it.
  Then we have seen in the last couple minutes how to find an equilibrium there.
  But how do we map back to the original game?
  
  
  \section{Prolongation}
  
  Well, given an abstract strategy $\wt{\omega}$, there is the concept of \emph{lifted strategies}.
  The idea is pretty simple:
  Given an information set $I$, the probability in the corresponding abstract information set for taking an abstract action in the abstract game simply has to be distributed somehow over the actions in the original game which would map onto that abstract action.
  There are no further requirements, so it would even be allowed to just put the full probability on one single action in the original game and omit all others.
  
  A slightly more refined concept is the one of \emph{undivided strategies}.
  For reasons of time-keeping I will not go into too much detail on that, but, loosely said, it means tha the probability of reaching a game state relative to an information set in the abstract game needs to be the same in the non-abstract game, as long as luck is neglected.
  This is a property which for many abstractions follows automatically from the lifted-strategy property, so for this talk it is enough to just think in terms of lifted strategies only.
  
  So, it is pretty easy to come up with a prolongation of abstract strategies.
  But how well does that work?
  
  
  \section{A bound on approximation quality}
  
  An important thing to know, which is especially new for somebody like me who is coming from the field of numerics of partial differential equations, is the so-called phenomenon of \emph{abstraction pathology}.
  If essentially means that even a strict refinement of an abstract game need not necessarily yield a strategy that is better.
  As it is written here, there are actually examples of zero-sum games where player~2 could win more against a player who plays a strategy that was prolongated from the fine game compared to the one from the coarse game.
  This fact kind of led to a little crisis in game theory, because it makes one completely question if this whole abstraction concept for games makes sense at all.
  
  However, in 2014, Kroer and Sandholm were actually able to give a proof on the solution quality of abstracted strategies.
  The proof itself is pretty technical, but mostly relies on relatively elementary inductive arguments.
  But, the statement is simple, here for the case where there is no luck component at all, i.\,e. there is no player~0 present:
  If there is a Nash equilibrium $\wt{\sigma}$ of the abstract game $\wt{G}$, then any undivided lifted strategy $\sigma$ of $\wt{\sigma}$ is an $\eps$-Nash equilibrium, where $\eps$ is bounded by the approximation quality of the utility function.
  If there is a chance player~0, then pretty much the same result holds true, but their bound on $\eps$ becomes a little more involved.
  
  So, this is pretty exciting, because it at least opens the stage for game abstraction.
  
  
  \section{Challenge: Computing abstractions}
  
  However -- at least In my opinion -- it nonetheless is not yet entirely clear how to come up with good abstractions.
  So far, in the literature, mostly clustering-based algorithms dominate, which act level-by-level.
  However, they are purely driven by heuristics and it is hard to bound their approximation quality.
  
  A more direct approach are integer programming approaches, which pretty much incorporate a bound on the approximation quality in their formulation.
  However, they are pretty expensive to solve and therefore not always an option.
  
  In short, this is yet another field that could lead to some interesting research questions.
  Even if the abstraction problem might be intractable for classical methods, this might be an interesting stage for more powerful machine learning methods, too.
  
  That also brings us to the end of this talk.
  We have seen that there are promising developments in algorithmic game theory, which are even able to compete with novel machine learning methods, in particular also artificial neural networks, while being potentially more tractable for classical analysis methods.
  However, most of the recent work has been done by computer scientists, who did a formidable job, but who also left quite a few questions open that could even pose interesting research challenges for mathematicians and in particular numerical mathematicians.
  
  Thank you very much for your attention.
  
\end{document}

