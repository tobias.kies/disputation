Equilibrium finding and abstraction in games of imperfect information
=====================================================================

In early 2017, roughly one year after the debut of Google's famous AI AlphaGo,
the field of AI hit yet another significant milestone when the computer program
Libratus became the first software to reach a super-human skill level at playing
the game of heads-up no-limit Texas hold 'em poker. A remarkable property of
Libratus is that it is almost entirely based on game theoretic principles only
and that it does not rely on popular machine learning methods such as artificial
neural networks at all. Moreover, the involved solution methods directly generalize
to the larger class of imperfect information games, which finds application in
numerous real-world applications that go beyond games in the recreational sense.

Inspired by this breakthrough, this talk aims at giving a brief introduction to various
basics and core concepts relevant to the theory and practical solving of imperfect
information games. Afterwards, the subject of algorithmic computation of equilibrium game
strategies is visited in greater detail, as well as a framework for abstracting games
into smaller ones.
