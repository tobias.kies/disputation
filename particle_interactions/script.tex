% File with general header information.
% Like all the packages that we need to include and very common commands
% that we use.
\input{inc/header.tex}

% File with "general commands". Those are commands  and environments
% that we find useful throughout many papers (and other kinds of documents).
\input{inc/generalCommands.tex}

% File with "specific commands". Those are commands that may be rather
% specific to the current document (like shortcuts to some notation).
\input{inc/specificCommands.tex}

% Start of the actual document.
\begin{document}
  \selectlanguage{english}
  
  
  \section{Intro}
  
  As you can see, my PhD research is on \emph{``Gradient Methods for Membrane-Mediated Particle Interactions.''}
  
  Roughly speaking, this means that I am concerned with how particles that may be attached to a biomembrane, such as for example certain proteins or nano particles, interact with each other when the driving force between them is mediated indirectly through elastic forces on the membrane.
  More precisely, I am not only interested in the pure evaluation or quantification of this interaction, but much rather I would like to be able to compute derivatives of the interaction, which then could for example be used to derive efficient methods for computing optimal particle configurations.
  And all of this should ideally happen in an as general framework as reasonably possible in order to make it as useful as possible.
  
  So much for a rough intuition on the topic.
  Of course this would not be very mathematical research if I wouldn't make this a little more precise.
  So here we go:
  
  
  \section{Parametric fourth-order problem}
  
  The first crucial component to an interaction model as I just described it is the elastic energy of the membrane.
  A framework which can describe a wide range possible scenarios is given by this energy representation.
  The energy may depend on the state of particles, which is encoded by a finite-dimensional particle configuration vector $\p$, and a perturbation function $u \in H^2$, which induces a new membrane shape.
  The actual energy is then determined by the integral of a fixed energy kernel~$\rho$, which takes derivatives of $u$ up to second order, over a domain that may be configuration dependent.

  Also, for those of you who are already familiar with the topic, this representation includes for example the well-known Monge-gauge but also a variety of purely nonlinear energies and non-planar geometries.
  This makes it an interesting object to study.
  
  Besides the membrane's elastic energy, the other crucial component is how particles are modeled to interact with the membrane locally.
  
  For our models here we shall simply assume that there is some local particle--membrane interface $\Gamma$ given, which is either a discrete point set or a curve on the membrane, and which of course may depend on the particle configuration.
  Along this interface either point value constraints are imposed of so-called curve constraints.
  In either case, one might be thinking of a particle of some sort locally prescribing the shape of the membrane.
  This then naturally yields a space of admissible membrane shapes, which is given as an affine subspace of $H^2$ that encodes the local constraints plus maybe some configuration-independent ones.
  
  Both components together yield the \emph{interaction potential}.
  This is the big object of interest because it encodes the elastic energy associated to a fixed particle configuration.
  More precisely, given a configuration~$\p$, the interaction potential is defined as the minimum elastic energy that can be obtained while still respecting the local constraints that are associated to that particle configuration.
  In particular, it yields a description of the membrane-mediated mechanical interaction between particles on a membrane.
  
  Our goal here is to minimize the interaction potential, which in turn should tell us something about stable particle configurations.
  Ideally we want to use gradient methods.
  
  
  \section{Differentiability of $\cE$, part I}
  
  Naturally, the first question of course is: Is the interactio potential~$\cE$ differentiable?
  This is indeed a non-trivial question because the chain rule cannot be applied to the interaction potential~$\cE$ due to the $\p$-dependence of the domain $\Omega$.
  
  The answer two this is positive -- else I would not be showing you this -- and the proof of this relies essentially on two steps:
  
  The first ingredient is the proof that so-called trace-preserving maps exist.
  Assume that in a neighborhood of $\p_0$ there is a sufficiently smooth family of maps between the particle--membrane interfaces~$\Gamma$ -- this would simply have to come from the model formulation -- then it is possible to prove the existence of a smooth extension~$\cX$ of this map, such that the sets of admissible membrane shaps are isomorphic by virtue of this mapping.
  Besides the map $\cX$, which encodes the deformation of the 2D-domain $\Omega$, there is also a map~$\xi$ involved, which encodes the deformation of the bondary values.
  
  
  \section{Differentiability of $\cE$, part II}
  
  This leads to the second part: Knowing, that all admissible spaces are locally isomorphic, the energy may be smoothly transformed onto a common reference domain, thus eliminating their dependence on $\p$.
  
  Under some typical uniqueness conditions it then follows from a direct application of the implicit function theorem that the interaction potential is differentiable.
  This is of course mostly due to the fact that the transformation is able to preserve or rather translate some of the problem's smoothness.
  
  As comforting as this result may be, it is not yet useful because we still do not know how to actually compute the gradient of the interaction potential.
  So how is that done?
  
  
  \section{Volume integral representation of $\partial_\q \cE$}
  
  In order to deal with that problem, we can use the so-called velocity method.
  The idea is relatively simple: In order to evaluate the derivative in some direction $\q$, we first construct vector fields $V$ and $w$ that capture how the domain and the bounary values want to change along the membrane--particle interface $\Gamma$ when being perturbed in direction $\q$.
  That is in principle relatively simple to achieve, at least numerically.
  
  Those vector fields then naturally induce a family of trace preserving maps $\cX$ for which we also know what the $\q$-derivative looks like.
  
  This information is then used together with standard means of matrix calculus to carry out the differentiation in the transformed interaction potential explicitly.
  And as a result we arrive at an expression that is feasible for application in numerical schemes because it only involves quantities that we either know or for which at least appropriate error estimates can be derived, namely the vector fields $V$ and $w$ as well as the optimal membrane shape $u$ in $\p_0$.
  
  All of that assumes that we have a proper discretization for $u$, of course.
  So let's check what one could do there.
  
  
  \section{Discretization}
  
  Suppose that we have a problem with curve constraints and that we are considering a problem for a fixed particle position.
  This means, we have a domain $\Omega$, which has some curve in its interior -- here depicted as a circle -- that corresponds to the membrane--particle interface and along which we would like to prescribe the function value and the first derivative.
  And, among all functions satisfying those conditions, we would like to thoose the one minimizing the elastic energy~$J_0$, which here is assumed to be linear and elliptic, with the bilinear form being the sum of positive-semidefinite volume integrals with sufficiently smooth coefficients and derivatives up to second order.
  
  The ansatz taken here is to use a structured grid of unfitted quadrilaterals.
  For such grids it is relatively easy still to implement finite element spaces which are proper subspaces of $H^2(\Omega)$.
  However, it obviously comes at the cost that the domain is not necessarily resolved well and, in particular, it becomes more difficult to prescribe the appropriate boundary values along the given interface.
  
  One way to deal with this is to use the so-called ``fictitious domain stabilized Nitsche metod'', which was mostly introduced by Eric Burman and Peter Hansbo and relies on some very fundamental insights from Joachim Nitsche.
  The literature usually only considers first order methods, but the approach readily generalizes to the setting as it is given here.
  
  The idea behind the method essentially is to augment the original energy by three terms.
  The first term is a simple penalty term, which is used to enforce the correct function values and derivatives along the interface $\Gamma$ and whose weight depends scales with the size of the grid.
  The second term is the contribution of Nitsche, who found out that it is also necessary to take into accoun certain additional terms in order to make the discretization scheme consistent with the exact solution of the problem.
  The third and final term is a stabilization term which penalizes jumps of higher order derivatives along faces of boundary elements.
  Those are the red edges in the drawing over here.
  This term is necessary in order to avoid a degeneration of the discretization for vanishing element supports.
  

  \section{Error estimates}
  
  Using that discretization scheme and following the ideas of Burman and Hansbo, it is possible to prove the typical standard error estimate in the energy norm, provided sufficient regularity of the exact solution $u$.
  
  Furthermore, if $G_i^h$ is an approximation of the $i$-th derivative which simply results from replacing $u$ by $u_h$ in the corresponding integral expression, it is possible to prove an error bound on the gradient approximation error in terms of the discretization error in the energy norm and the $H^{2,\infty}$-norm of the constructed vector fields $V$ and $w$.
  In particular, this suggests to try to construct $V$ and $w$ in such a way that they are as smooth as possible.
  
  Of course, those results could also be verified numerically, and similar results hold for point value constraints as well.
  This altogether underlines the numerical feasibility of the whole approach, at least in the considered subclass of elasticity problems. In particular, this opens the stage for applying gradient methods to problems that involve the interaction potential.
  For example, one might simply try to minimize the interaction potential using a perturbed gradient method, which then can yield useful information on energetically favorable particle configurations.
  
  So far, all of this has been relatively abstract.
  Now it is time to reward ourselves with some examples and lots of pretty pictures:


  \section{Isotropic inclusions in Monge-gauge}
  
  The first example here considers an almost flat-membrane with a geometrically linearized elastic energy, also known as the Monge-gauge.
  In the example, this flat membrane is deformed by isotropic  inclusions, as depicted here.
  I.\,e. those are cone-like objects that pinch through the membrane and thus force the membrane to take a fixed shape along the membrane--particle interface.
  Mathematically, this is modeled by a circular interface $\Gamma$ for each particle, and each particle is on top equipped with two degrees of freedom, which encode the lateral position of the particle.
  The boundary values are kept constant.
  
  In the pictures you see a top-view on iterates generated from a perturbed gradient method applied to nine identical inclusions starting from a more or less arbitrary initial state.
  The coloring encodes simply the elevation of the membrane shape, where yellow means highly elevated, and blue means low elevation.
  You see that these particles quickly form a rather uniform pattern.
  This is in the range of what is expectable, because it is already well-known from literature, that such particles have pairwise repulsive interactions, which makes a even distribution over the domain of computation a plausible result.


  \section{Anisotropic scaffolds}
  
  It is also possible to apply our results to les regular shapes, as is for example done here where the isotropic inclusions are replaced by anisotropic scaffolds with elliptical membrane--particle interfaces.
  In this case each of these scaffolds has now an additional degree of freedom due to the asymmetry in shape, which encodes the rotation of the particle in the plane.
  
  Using elsewise the same optimization setup as before, the interaction of these particles is now vastly different:
  Over the course of a few hundred iterations, those particles actually exhibit attractive forces, which makes them form a more interesting pattern.
  This also fits with results from literature that already had observed the non-negligibility of anisotropy in particles shapes before.
  
  
  \section{FCHo2 F-BAR domains}

  A more realistic example is the clustering of FCHo2 F-BAR domains.
  Those are proteins that are suspected to have an impact during the early stages of clathrin-mediated endocytosis by forming functional domains.
  
  In order to model this situation, a fully atomistic representation of the protein is extracted from the protein data base, which is shown in the picture here.
  Then the position of those residues which are the locations where the membrane would be the most likely to bind are extracted.
  Those are shown in blue.
  In the mathematical model, those positions then induce point-value constraints to tell how a particle restricts the membrane locally.
  In order to avoid degenerate behavior, the elastic interaction energy is on top augmented by a direct particle-particle interaction which is loosely based on a Lennard--Jones repulsion.
  An invocation of the same optimization method like in the previous application examples to an initial configuration of proteins with a kink then reveals that the formation of such linear macro-structures is indeed stable and plausible.
  Altogether, this shows potential for further numerical experiments.


  \section{Outlook: Statistical mechanics}
  
  One promising direction for future work is the step away from purely determinidstic optimization towards the extraction of relevant quantities using statistical mechanics.
  
  Essentially, the idea is that a thermodynamics-like behavior of particles is given by an overdamped Langevin-dynamics equations, which is closely related to a perturbed gradient flow.
  An emphasis is on ``thermodynamics-like'' because there are some model assumptions implicit to this representation, which ideally should get discussed, too.

  Based on these dynamics, it then becomes potentially feasible to do computations on free energies, which are are a physically relevant quantity.
  That this is possible comes from the fact that, under some conditions, there is a close link between trajectories of the overdamped Langevin equation and differences of free energies.
  Hence, our gradient results might have the potential to lead to new insights in the statistical mechanics of particles in membranes.
  

  This concludes my talk.
  Thank you very much!
  
  
\end{document}

