\documentclass[ucs,10pt]{beamer}

\input{inc/head_pres}
\input{inc/specificCommands}

% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.
%
% Modified by Tobias G. Pfeiffer <tobias.pfeiffer@math.fu-berlin.de>
% to show usage of some features specific to the FU Berlin template.

% remove this line and the "ucs" option to the documentclass when your editor is not utf8-capable
\usepackage[utf8x]{inputenc}    % to make utf-8 input possible
\usepackage[english]{babel}     % hyphenation etc., alternatively use 'german' as parameter
\usepackage{nth}

\include{fu-beamer-template}  % THIS is the line that includes the FU template!

%\usepackage{arev,t1enc} % looks nicer than the standard sans-serif font
% if you experience problems, comment out the line above and change
% the documentclass option "9pt" to "10pt"

% image to be shown on the title page (without file extension, should be pdf or png)
%~ \titleimage{./gfx/title.png}

\title%[Unfitted FEM for particle interaction] % (optional, use only with long paper titles)
{Gradient Methods for Membrane-Mediated \newline Particle~Interactions} % The space between to lines looks weirdly close when there is a line break. How can the problem be solved more elegantly?

%\subtitle{Include Only If Paper Has a Subtitle}

\author{Tobias Kies}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}
% - Keep it simple, no one is interested in your street address.

\date % (optional, should be abbreviation of conference name)
{May \nth{23}, 2019}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

%\subject{Theoretical Computer Science}
% This is only inserted into the PDF information catalog. Can be left
% out.

% you can redefine the text shown in the footline. use a combination of
% \insertshortauthor, \insertshortinstitute, \insertshorttitle, \insertshortdate, ...
\renewcommand{\footlinetext}{\insertshortinstitute, \insertshorttitle, \insertshortdate}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


{\renewcommand{\arraystretch}{1.6}%
\newcommand{\gooditem}[1]{\setbeamercolor{item}{fg=green}\item #1}
\newcommand{\baditem}[1]{\setbeamercolor{item}{fg=red}\item #1}
\newcommand{\neutralitem}[1]{\setbeamercolor{item}{fg=structure}\item #1}
\newcommand{\futureitem}[1]{\setbeamercolor{item}{fg=yellow}\item #1}

\newcommand{\sectionslide}[1]{%
\begin{frame}[plain]%
  \begin{center}\textbf{\huge\textcolor{fu-light-blue}{#1}}\end{center}%
\end{frame}%
}

\newcommand{\iarrow}{\raisebox{0.1em}{\textcolor{fu-blue}{$\blacktriangleright$}}}
\newcommand{\arlb}{\raisebox{0.1em}{\textcolor{emph-color}{$\blacktriangleright$}}}
\newcommand{\ardb}{\raisebox{0.1em}{\textcolor{fu-blue}{$\blacktriangleright$}}}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{Parametric fourth-order problem}
  \begin{itemize}
    \item \ssubcapbf{Elastic energy}
      \begin{align*}
        J(\p; u) & := \int_{\Omega(\p)} \rho(x, u, Du, D^2 u) \d{x}
      \end{align*}
      \begin{align*}
        \text{membrane shape } \quad & u \in H^2(\Omega(\p)),\ \Omega(\p) \subseteq \R^2
        \\ \text{particle configuration } \quad & \p \in \cD \subseteq \R^{n \times k}
        %~ \\ J(\p; u) & := \int_{\Omega(\p)} \rho(x, u(x), Du(x), D^2 u(x)) \d{x}
      \end{align*}

    \pause
    \item \ssubcapbf{Local constraints}
      \vspace{-0.5em}
      \begin{align*}
        %~ \text{particle--membrane interface }\quad
        \Uad(\p) & := \left\{ u \in H_\ast^2(\Omega(\p)) \mid T(\p;v) = g(\p) \right\}
        \\[0.5em] & \Gamma(\p) \subseteq \partial\Omega(\p)
        \\ \text{curve constr. } \quad & u|_{\Gamma(\p)} = g_1(\p), \ \partial_\nu u|_{\Gamma(\p)} = g_2(\p)
        \\ \text{point value constr. } \quad & u|_{\Gamma(\p)} = g(\p)
      \end{align*}

    \pause
    \item \ssubcapbf{Interaction potential}
      \begin{align*}
        \cE(\p) = J(\p,u(\p)) = \inf_{v \in \Uad(\p)} J(\p,v) \ \longrightarrow \min!
      \end{align*}
  \end{itemize}
\end{frame}

\newcommand*\Isom{%
  \xrightarrow{\raisebox{-0.25em}{\smash{\ensuremath{\sim}}}}%
}

\begin{frame}[allowframebreaks]{Differentiability of $\cE$, part}
  \begin{align*}
    \dd{\p} \cE(\p_0) = \dd{\p} J(\p_0, u(\p_0)) = \text{\ \large \textbf{?}}
  \end{align*}

  \begin{itemize}
    \item \ssubcapbf{Isomorphisms}
      \begin{align*}
        & \psi(\p)\colon \Gamma(\p_0) \Isom \Gamma(\p) \in C^{m+1}
        \\[1.25em] \Longrightarrow\qquad& \exists\colon\ \cX(\p)\colon \Omega(\p_0) \Isom \Omega(\p) \in C^m
        \\[0.75em] & \phantom{\exists\colon\ }T(\p, u \circ \cX(\p)^{-1}) = T(\p_0, u) \circ \cX(\p)^{-1}%|_{\Gamma(\p)} \ %\forall{u \in H^2(\Omega(\p_0))}
        \\[1.25em] \Longrightarrow\qquad& \phi\colon \Uad(\p_0) \Isom \Uad(\p), \ v \mapsto (v + \xi(\p)) \circ \cX(\p)^{-1}
        %~ \\[0.75em] \text{where}\hspace{12.4mm} & T(\p_0; \xi(\p)) = g(\p) \circ \cX(\p)|_{\Gamma(\p_0)} - g(\p_0)
      \end{align*}
  \end{itemize}

  \framebreak
  \begin{itemize}[itemsep=2.5em]
    \item \ssubcapbf{Transformed elastic energy}
      \begin{align*}
        \wt{J}(\p; u) & = J(\p; \phi(u)) \in C^{m-2}, \ \textcolor{red}{u \in \Uad(\p_0)}
        \\[0.75em] \wt{u}(\p) & = \textstyle\argmin_{v \in \Uad(\p_0)} \wt{J}(\p; v) %= u(\p) \circ \cX(\p) - \xi(\p)
        \\[0.75em] \cE(\p) & = \wt{J}(\p; \wt{u}(\p))
      \end{align*}

    \item \ssubcapbf{Differentiable,} \textbf{if:} $u(\p_0)$ unique and $J_{uu}(\p_0, u_0)$ invertible
      \begin{align*}
        \textbf{Implicit function theorem} \implies \cE \in C^{m-3}
      \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{Volume integral representation of $\partial_{i} \cE$}
  \begin{itemize}[itemsep=2em]
    \item \ssubcapbf{Velocity method} $V \in H^2(\Omega(\p_0), \R^2)$
      \begin{align*}
        & V|_{\Gamma(\p_0)} = \partial_{i} \psi(\p_0), \qquad DV\nu|_{\Gamma(\p_0)} = \partial_{i} \left(\nu \circ \psi(\p_0)\right)
        \\[1.0em] \Longrightarrow & \ \exists{\cX\colon} \ \partial_{i} \cX(\p_0) = \dd{t} \left.\cX(\p_0 + te_i)\right|_{t=0} = V
      \end{align*}
      %~ \begin{align*}
        %~ V & \in H^2(\Omega(\p_0), \R^2) & w & \in H^2(\Omega(\p_0))
        %~ \\[0.75em] V|_{\Gamma(\p_0)} & = \partial_{i} \psi(\p_0) & w|_{\Gamma(\p_0)} & = \partial_{i}\left( g_1(\p_0) \circ \psi(\p_0) \right)
        %~ \\[0.75em] DV\nu|_{\Gamma(\p_0)} & = \partial_{i} \left(\nu \circ \psi(\p_0)\right) & \partial_\nu w|_{\Gamma(\p_0)} & = \partial_{i}\left( g_2(\p_0) \circ \psi(\p_0) \right)
        %~ \\[0.75em] \leadsto \partial_{i} \cX(\p_0) & = V
      %~ \end{align*}
      %~ \begin{align*}
        %~ V \in & H^2(\Omega(\p_0), \R^2), \ w \in H^2(\Omega(\p_0))
        %~ \\[0.75em] V|_{\Gamma(\p_0)} & = \partial_{i} \psi(\p_0)
        %~ \\[0.75em] DV\nu|_{\Gamma(\p_0)} & = \partial_{i} \left(\nu \circ \psi(\p_0)\right)
        %~ \\[0.75em] T(\p_0; w) & = \partial_{i}\left( g(\p_0) \circ \psi(\p_0) \right)
      %~ \end{align*}
    \item
      \ssubcapbf{Matrix calculus}
  \end{itemize}

  \vspace{-1.5em}
  \begin{align*}
    \implies \partial_{i} \cE(\p_0)
    & = \int_{\Omega(\p_0)} \div(V) \rho + \rho_x V + \rho_y w + \rho_z \cdot (\nabla w - DV\textcolor{red}{\nabla u_0})\, \d{x}
    \\ & \qquad + \int_{\Omega(\p_0)} \rho_Z : \left(D^2w - D^2V\textcolor{red}{\nabla u_0} - \operatorname{Sym}(\textcolor{red}{D^2u_0} \cdot DV)\right)\, \d{x}
  \end{align*}
\end{frame}

\begin{frame}{Discretization}
  \begin{align*}
    \min_{v \in H^2(\Omega_0)} J_0(v) & = \frac{1}{2} a_{\Omega_0}(v,v) - \ell_0(v)
    %~ , \quad a_{\Omega_0}(v,w) = \sum_{i=1}^n \left \langle \sum_{\abs{\alpha} \leq 2} c_\alpha^i \partial^\alpha v, \sum_{\abs{\alpha} \leq 2} c_\alpha^i \partial^\alpha w  \right\rangle_{L^2(\Omega_0)}
    %~ , \quad a_{\Omega_0}(v,v) = \sum_{i=1}^n \norm{\textstyle \sum_{\abs{\alpha} \leq 2} c_{i,\alpha} \partial^\alpha v}_{L^2(\Omega_0)}^2
    , \qquad v|_{\Gamma} = g_0, \ \partial_\nu v|_{\Gamma} = g_1
  \end{align*}

  \hspace*{+84mm}\raisebox{-52mm}[0mm][0mm]{\includegraphics[width=0.30\textwidth]{gridDiagram.pdf}}

  \pause
  \ssubcapbf{Fictitious domain stabilized Nitsche method}, $\cS \subseteq H^{2}$ piecewise $\cQ_k$
  \begin{align*}
    \forall{v \in \cS\colon} \ \left(a_{\Omega_0} + \aPen + \aNit + \aStab\right)(u_h,v) = \left(\ell_0 + \ell_{\text{pen}} + \ell_{\text{Nit}}\right)(v)
  \end{align*}
  \begin{minipage}{0.55\textwidth}
    \begin{align*}
      \aPen(v,w) & = \lambda \left( \left\langle h^{-3} v, w\right\rangle_{L^2(\Gamma)} + \left\langle h^{-1} \nabla v, \nabla w\right\rangle_{L^2(\Gamma)} \right)
      \\ \aNit(v,w) & =
        %~ \sum_{\substack{\abs{\alpha} \leq 3, \abs{\beta}\leq 1 \\ \abs{\alpha} + \abs{\beta} \leq 3}}
        \sum_{\abs{\alpha}, \abs{\beta} \leq 3}
        \int_{\Gamma} c^{\Gamma}_{\alpha\beta} \partial^\alpha v \, \partial^\beta w \d{x}
      \\ \aStab(v,w) & = \textcolor{red}{\sum_{F \in \cF}} \sum_{j=2}^k c_j \left\langle h^{2j-3} \llbracket \partial_\nu^j v\rrbracket, \llbracket \partial_\nu^j w\rrbracket\right\rangle_{L^2(F)}
    \end{align*}
  \end{minipage}
\end{frame}

\begin{frame}{Error estimates}
  \begin{minipage}{0.8\textwidth}
    \ssubcapbf{Discretization error}
    \begin{align*}
      \norm{u - u_h}_{H^2(\Omega_0)} \leq c \norm{h^{k-1} D^{k+1} \wt{u}}_{L^2(\wt{\Omega})}
    \end{align*}

    \vspace{0.5em}
    \ssubcapbf{Gradient approximation error}
    \begin{align*}
      \norm{\partial_i \cE(\p) - G_i^h} \leq c(u) \norm{(V,w)}_{H^{2,\infty}} \norm{u - u_h}_{H^2(\Omega_0)}
    \end{align*}

    \vspace{1.5em}
    $\leadsto$ \textbf{perturbed gradient methods}
  \end{minipage}

  \hspace*{+80mm}\raisebox{15mm}[0mm][0mm]{\includegraphics[width=0.30\textwidth]{convergence_curve_inexact2_H2.pdf}}
  \hspace*{+80mm}\raisebox{-21mm}[0mm][0mm]{\includegraphics[width=0.30\textwidth]{gradients_curve_inexact.pdf}}
\end{frame}

%~ \begin{frame}{Numerical experiments}
  %~ ...
%~ \end{frame}

\begin{frame}{Isotropic inclusions in Monge-gauge}
  \vspace{1em}

  $\Gamma_i$ circular, $\rho(u) = (\Delta u)^2$
  \\ $\p \in (\R^2)^n$
  \\ curve constr. $g|_{\Gamma_i} \equiv \operatorname{const}$
  \\ \ 
  \\ iter. $0,\dots, 8$

  \vspace{1.5em}

  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_1.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_2.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_3.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_4.eps}

  \vspace{0.5em}

  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_5.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_6.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_7.eps}
  \includegraphics[width=0.23\textwidth]{isotropicMongeGauge_iterate_8.eps}

  \hspace*{+48mm}\raisebox{65mm}[0mm][0mm]{\includegraphics[width=0.30\textwidth]{04_inclusion.pdf}}
  \hspace*{+83mm}\raisebox{65mm}[0mm][0mm]{\includegraphics[width=0.25\textwidth]{isotropicMongeGauge_iterate_0.eps}}
\end{frame}

\begin{frame}{Anisotropic scaffolds}
  \vspace{1em}

  $\Gamma_i$ elliptical
  \\ $\p \in (\R^3)^n$
  \\ curve constr. $g|_{\Gamma_i}$ %\equiv \operatorname{const}$
  \\ \ 
  \\ iter. 0, 10, 25, 50, 75, 100, 150, 200, 250

  \vspace{1.5em}

  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_1.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_2.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_3.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_4.eps}

  \vspace{0.5em}

  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_5.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_6.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_7.eps}
  \includegraphics[width=0.23\textwidth]{anisotropicMongeGauge_iterate_8.eps}

  \hspace*{+45mm}\raisebox{70mm}[0mm][0mm]{\includegraphics[width=0.30\textwidth]{06_scaffold.pdf}}
  \hspace*{+83mm}\raisebox{65mm}[0mm][0mm]{\includegraphics[width=0.25\textwidth]{anisotropicMongeGauge_iterate_0.eps}}
\end{frame}

\begin{frame}{FCHo2 F-BAR domains}
  $\Gamma_i$ discrete
  \\ $\p \in (\R^4)^n$
  \\ point constr. $g|_{\Gamma_i}$ %\equiv \operatorname{const}$
  \\ $\wt{\cE}(\p) = \cE(\p) + \cE_{\text{p--p}}(\p)$
  \\ \ 
  \\ iter. 0, 10, 25, 50, 75, 100, 150, 200, 300

  \vspace{0.5em}

  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_1.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_2.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_3.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_4.eps}

  \vspace{0.5em}

  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_5.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_6.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_7.eps}
  \includegraphics[width=0.23\textwidth]{FCHo2_iterate_8.eps}

  \hspace*{+39mm}\raisebox{70mm}[0mm][0mm]{\includegraphics[width=0.37\textwidth]{FCHo2_side.png}}
  \hspace*{+83mm}\raisebox{65mm}[0mm][0mm]{\includegraphics[width=0.25\textwidth]{FCHo2_iterate_0.eps}}
\end{frame}

\begin{frame}{Outlook: Statistical mechanics}
  \begin{itemize}[itemsep=0.75em]
    \item \ssubcapbf{Free energy}
      \begin{align*}
        %~ e^{-\beta F(\lambda)} 
        F(\lambda)
        & = -\frac{1}{\beta} \log\left(\int_{\cD} e^{-\beta \cE(\lambda, \p)} \d{\p}\right)
      \end{align*}
      
    \item \ssubcapbf{Overdamped Langevin dynamics}
      \begin{equation*}
        \d{\p(t)} = -\nabla \cE(\p) \d{t} + \sqrt{\frac{2}{\beta}} \d{W_t}
      \end{equation*}

    \item \ssubcapbf{Energy perturbation method}
      \begin{align*}
        %~ e^{-\beta \Delta F}
        \Delta F
        & = \lim_{T\to \infty} -\frac{1}{\beta} \log\left(\frac{1}{T} \int_0^T e^{-\beta \Delta \cE(\p(t))} \d{t}\right)
      \end{align*}
  \end{itemize}
\end{frame}

%\begin{frame}{Outline}
%  \tableofcontents
%  % You might wish to add the option [pausesections]
%\end{frame}



%~ \begin{frame}[allowframebreaks]{Literature}
  %~ \scriptsize
  %~ \nocite{BroSan18}
  %~ \nocite{Johanson13}
  %~ \nocite{Kuhn53}
  %~ \nocite{Nash51}
  %~ \nocite{Neumann28}
  %~ \nocite{Nesterov05}
  %~ \nocite{LaLuMo11}
  %~ \nocite{GiPeSa12}
  %~ \nocite{KroSan14}
  %~ \bibliographystyle{plain}
  %~ \bibliography{references}
%~ \end{frame}

\pagenumbering{gobble}

\begin{frame}{}
  \vfill
  \begin{center}
    \textbf{\huge Thank you! \ \ \ }
  \end{center}
  \vfill
\end{frame}
\end{document}
